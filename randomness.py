import numpy as np
import matplotlib.pyplot as plt
from keygen import *

N = 500
W, H = N, N

passx = ['aniqurrahman', '12345', 'mayukh', 'pyxel', 'securesnaps', 'ashshamsi']
degree= int(1.5*W*H)
plt.hold(True)
full_sum = np.zeros(max(W,H))

for j in range(6):
    TUPLE= key_init(passx[j], W, H)
    total = np.zeros(max(W,H))
    a = TUPLE
    b = TUPLE

    for j in range(degree):
        a=b
        b= next_tuple(a)
        L = len(a)
        for i in range(L):
            a1, a2 = a[i][0]%H, a[i][1]%W
            b1, b2 = b[i][0]%H, b[i][1]%W
            total[a1]= total[a1]+1
            total[a2]= total[a2]+1
            total[b1]= total[b1]+1
            total[b2]= total[b2]+1

    for i in range(N):
        full_sum[i] = full_sum[i] + total[i]

    plt.plot(list(range(N)), total)

plt.plot(list(range(N)), np.multiply(full_sum, 0.166666), 'k--')


plt.show()
